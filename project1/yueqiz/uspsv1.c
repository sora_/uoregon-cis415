//Yueqi Zhu
//yueqiz
//CIS 415 Project 1
//This is my own work.

#include <unistd.h>
#include <stdlib.h>
#include "p1fxns.h"
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/wait.h>

//#include <stdio.h>
#define UNUSED __attribute__((unused))


int main(int argc, char *argv[]){
	int fd,i,t;
	char buf[1024];
	char temp[1024];
	char *command[1024];
	UNUSED int quantum;
	int count;
	pid_t pid[1024];
	count=0;
	quantum=0;
	if (argc==1){
		fd=0;
		if (getenv("USPS_QUANTUM_MSEC")!=NULL){
			quantum=p1atoi(getenv("USPS_QUANTUM_MSEC"));
		}
		else{
			p1perror(1,"error: quantum not defined\n");
			exit(1);
		}
	}
	else if (argc==2){
		if(p1strneq(argv[1],"--quantum=",10)){
			fd=0;
			quantum=p1atoi(argv[1]+10);
		}
		else{
			if ((fd=open(argv[1],O_RDONLY))<0){
				p1perror(1,"file open failed!\n");
				exit (1);
			}
			if (getenv("USPS_QUANTUM_MSEC")!=NULL){
				quantum=p1atoi(getenv("USPS_QUANTUM_MSEC"));
			}
			else{
				p1perror(1,"error: quantum not defined\n");
				exit(1);
			}
		}
	}
	else if (argc==3){
		if(p1strneq(argv[1],"--quantum=",10)){
			quantum=p1atoi(argv[1]+10);
		}
		if ((fd=open(argv[2],O_RDONLY))<0){
			p1perror(1,"file open failed!\n");
			exit (1);
		}
	}
	else if (argc>3){
		p1perror(1,"too many argument!\n");
		exit (1);
	}
	//puts("1");
	//puts(argv[1]);
	//fd=open(argv[1],O_RDONLY);
	//read(fd,buf,sizeof(buf));
	//puts(buf);
	
	while (p1getline(fd,buf,sizeof(buf))!=0){
		
		//puts(buf);
		t=0;
		i=0;
		//command[t]=(char*)malloc(sizeof(char)*1024);
		//printf("%d\n",p1getword(buf,0,temp));
		//puts(temp);
		//puts("0");
		while ((i=p1getword(buf,i,temp))!=-1){
			//puts(temp);
			command[t]=p1strdup(temp);
			//printf("%d",t);
			//puts(command[t]);
			t++;
			
		}
		//puts("1");
		//printf("%d",t);
		//puts(command[0]);
		//puts(command[1]);
		command[t]=NULL;
		i=0;
		while (command[t-1][i]!='\0'){
			i++;
		}
		i--;
		if (command[t-1][i]=='\n') command[t-1][i]='\0';
		//puts(command[t-1]);
		
		if ((pid[count]=fork())<0){
			p1perror(1,"fork failed!\n");
			exit (1);

		}
		if (pid[count]==0){
			if((execvp(command[0],command))<0){
				p1perror(1,"execvp failed!\n");
				exit (1);
			}
		}

		for (i=0;i<t;i++){
			free(command[i]);
		}
		count++;


	}
	//puts("2");

	while (count){
		wait(NULL);
		count--;
	}

	return 0;
}