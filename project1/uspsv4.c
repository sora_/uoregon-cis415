//Yueqi Zhu
//yueqiz
//CIS 415 Project 1
//This is my own work.
#include <unistd.h>
#include <stdlib.h>
#include "p1fxns.h"
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <time.h>
#include <sys/time.h>

#define UNUSED __attribute__((unused))

//#include <stdio.h>

int USR1_received = 0;		/* 0 means it is NOT true */
//int ALRM_received;
int stat[1024];
pid_t pid[1024];
int current;
int count;

void onusr1(UNUSED int sig) {
	//puts("hi0");
    USR1_received++;		/* anything non-zero is true */
}

void alarm_handler(UNUSED int sig) {
	p1putstr(1, "Pid\tName\tState\tVmSize\trchar\twchar\n");
	int i1,i,t,fd;
	char buf[100];
	char file[100];
	char temp[100];
	char name[100];
	char state[100];
	char vmsize[100];
	char rbyte[100];
	char wbyte[100];
	for (i=0;i<count;i++){
		if (stat[i])
			continue;
		file[0]='\0';
		p1strcat(file, "/proc/");
        p1itoa(pid[i], buf);
        p1strcat(file, buf);
        p1strcat(file, "/status");
        //puts(file);
        if ((fd = open(file, O_RDONLY))<0)
        	continue;
        while (p1getline(fd,buf,sizeof(buf))!=0){
        	//puts(buf);
        	i1=0;
			while (buf[i1]!='\0'){
				i1++;
			}
			i1--;
			if (buf[i1]=='\n') 
				buf[i1]='\0';
			//puts(buf);
        	t=p1getword(buf, 0, temp);
        	if ((p1strneq(temp, "Name:", 6))){
        		 p1getword(buf, t, name);
        	}
        	else if ((p1strneq(temp, "State:", 7))){
        		 p1getword(buf, t, state);
        	}
        	else if ((p1strneq(temp, "VmSize:", 8))){
        		 p1getword(buf, t, vmsize);
        	}

        }
        p1putint(1, pid[i]);
        p1putstr(1, "\t");
        p1putstr(1, name);
        p1putstr(1, "\t");
        p1putstr(1, state);
        p1putstr(1, "\t");
        p1putstr(1, vmsize);
        p1putstr(1, "\t");

        file[0]='\0';
		p1strcat(file, "/proc/");
        p1itoa(pid[i], buf);
        p1strcat(file, buf);
        p1strcat(file, "/io");
        //puts(file);
        if ((fd = open(file, O_RDONLY))<0)
        	continue;
        while (p1getline(fd,buf,sizeof(buf))!=0){
        	//puts(buf);
        	i1=0;
			while (buf[i1]!='\0'){
				i1++;
			}
			i1--;
			if (buf[i1]=='\n') 
				buf[i1]='\0';
			//puts(buf);
        	t=p1getword(buf, 0, temp);
        	if ((p1strneq(temp, "rchar:", 6))){
        		 p1getword(buf, t, rbyte);
        	}
        	else if ((p1strneq(temp, "wchar:", 6))){
        		 p1getword(buf, t, wbyte);
        	}

        }
        p1putstr(1, rbyte);
        p1putstr(1, "\t");
        p1putstr(1, wbyte);
        p1putstr(1, "\n");
	}





	kill(pid[current],SIGSTOP);
	current++;
	if (current==count)
		current=0;
	while (stat[current]){
		current++;
		if (current==count)
			current=0;
	}
	kill(pid[current],SIGCONT);
}



int main(int argc, char *argv[]){
	struct timespec tm = {0, 20000000};
	int fd,i,t;
	char buf[1024];
	char temp[1024];
	char *command[1024];
	int quantum;
	
	//pid_t pid[1024];

	if (signal(SIGUSR1, onusr1) == SIG_ERR) {
        p1perror(1,"signal error\n");
        exit (1);
    }
    if (signal(SIGALRM, alarm_handler) == SIG_ERR) {
        p1perror(1,"signal error\n");
        exit (1);
    }

	count=0;
	quantum=0;
	if (argc==1){
		fd=0;
		if (getenv("USPS_QUANTUM_MSEC")!=NULL){
			quantum=p1atoi(getenv("USPS_QUANTUM_MSEC"));
		}
		else{
			p1perror(1,"error: quantum not defined\n");
			exit(1);
		}
	}
	else if (argc==2){
		if(p1strneq(argv[1],"--quantum=",10)){
			fd=0;

			quantum=p1atoi(argv[1]+10);
		}
		else{
			if((fd=open(argv[1],O_RDONLY))<0){
				p1perror(1,"file open error\n");
        		exit (1);
			}
			if (getenv("USPS_QUANTUM_MSEC")!=NULL){
				quantum=p1atoi(getenv("USPS_QUANTUM_MSEC"));
			}
			else{
				p1perror(1,"error: quantum not defined\n");
				exit(1);
			}
		}
	}
	else if (argc==3){
		if(p1strneq(argv[1],"--quantum=",10)){
			quantum=p1atoi(argv[1]+10);
		}
		if ((fd=open(argv[2],O_RDONLY))<0){
			p1perror(1,"file open error\n");
        	exit (1);
		}
	}
	else if (argc>3){
		p1perror(1,"too many argument\n");
        exit (1);
	}

	while (p1getline(fd,buf,sizeof(buf))!=0){
		
		t=0;
		i=0;		
		while ((i=p1getword(buf,i,temp))!=-1){
			//puts(temp);
			command[t]=p1strdup(temp);
			t++;
			
		}
		command[t]=NULL;
		i=0;
		while (command[t-1][i]!='\0'){
			i++;
		}
		i--;
		if (command[t-1][i]=='\n') 
			command[t-1][i]='\0';
		//puts(command[t-1]);
		if ((pid[count]=fork())<0){
			p1perror(1,"fork error\n");
       		exit (1);
		}
		//puts("hi0");
		if (pid[count]==0){
			while (! USR1_received)
        		(void)nanosleep(&tm, NULL);
			if ((execvp(command[0],command))<0){
				p1perror(1,"execvp error\n");
        		exit (1);
			}
		}

		for (i=0;i<t;i++){
			free(command[i]);
		}
		count++;
		


	}
	

	for (i=0;i<count;i++){
		kill(pid[i],SIGUSR1);
	}


	for (i=0;i<count;i++){
		kill(pid[i],SIGSTOP);
	}
	
	
	for (i=0;i<count;i++)
		stat[i]=0;
	current=0;
	struct itimerval new_value;  
    new_value.it_value.tv_sec = quantum*1000/1000000;  
    new_value.it_value.tv_usec = quantum*1000%1000000;  
    new_value.it_interval.tv_sec = quantum*1000/1000000;  
    new_value.it_interval.tv_usec = quantum*1000%1000000;
    kill(pid[0],SIGCONT);
    setitimer(ITIMER_REAL, &new_value, NULL);

	int count1;
	count1=count;
	//printf("%d",count1);
	while (count1){
		//puts("hi");
		pid_t f;
		f=0;
		f=wait(NULL);
		//puts("hi");
		count1--;
		if (f>0){
			//puts("hi1");
			for (i=0;i<count;i++){
				if (pid[i]==f){
					//puts("hi2");
					stat[i]=1;
					current=i;
					//puts("hi1");
					break;
				}
			}
			//printf("%d",count1);
			if (count1){
				while (stat[current]){
					current++;
					if (current==count)
						current=0;
				}
				//char aaa[25];
				//p1itoa(current,aaa);
				//puts(aaa);
				kill(pid[current],SIGCONT);
				setitimer(ITIMER_REAL, &new_value, &new_value);
				//printf("%d",current);
			}
		}
		
	}

	return 0;
}