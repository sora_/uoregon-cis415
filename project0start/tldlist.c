//CIS415 PROJECT0
//Yueqi Zhu
//Duck id: yueqiz
//This is my own work

#include "date.h"
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

typedef struct tldnode{
	char *tld;
	long count;
	struct tldnode *left;
	struct tldnode *right;
} TLDNode;
typedef struct tldlist{
	Date *begin;
	Date *end;
	TLDNode *root;
} TLDList;
typedef struct tlditerator{
	TLDNode *node;
	struct tlditerator *next;
	struct tlditerator *prev;
} TLDIterator;

/*
 * tldlist_create generates a list structure for storing counts against
 * top level domains (TLDs)
 *
 * creates a TLDList that is constrained to the `begin' and `end' Date's
 * returns a pointer to the list if successful, NULL if not
 */
TLDList *tldlist_create(Date *begin, Date *end){
	TLDList *rtn;
	rtn=malloc(sizeof(TLDList));
	rtn->begin=begin;
	rtn->end=end;
	rtn->root=NULL;
	return rtn;
};

/*
 * tldlist_destroy destroys the list structure in `tld'
 *
 * all heap allocated storage associated with the list is returned to the heap
 */
//done

static void distroy_tree(TLDNode *node){
	if (node==NULL){
		return;
	}
	distroy_tree(node->left);
	distroy_tree(node->right);
	free(node->tld);
	free(node);
}
void tldlist_destroy(TLDList *tld){
	 distroy_tree(tld->root);
	free(tld);
} 

/*
 * tldlist_add adds the TLD contained in `hostname' to the tldlist if
 * `d' falls in the begin and end dates associated with the list;
 * returns 1 if the entry was counted, 0 if not
 */
 //helper functions
 
static long max(long a, long b){
 	if (a>b){
 		return a;
	 }
	 else{
	 	return b;
	 }
 }
 
static long height(TLDNode *n){
 	if (n==NULL){
 		return -1;
	 }
	 else{
	 	return max(height(n->left),height(n->right))+1;
	 }
 }
 
static TLDNode *rotateright(TLDNode *current){
 	TLDNode *temp;
 	temp=current->left;
 	current->left=temp->right;
 	temp->right=current;
 	current=temp;
 	return current;
 }
 
static TLDNode *rotateleft(TLDNode *current){
 	TLDNode *temp;
 	temp=current->right;
 	current->right=temp->left;
 	temp->left=current;
 	current=temp;
 	return current;
 }
 
static TLDNode *rotatelr(TLDNode *current){
 	current->left=rotateleft(current->left);
 	current=rotateright(current);
 	return current;
 }
 
static  TLDNode *rotaterl(TLDNode *current){
 	current->right=rotateright(current->right);
 	current=rotateleft(current);
 	return current;
 }
 
static TLDNode *balance(TLDNode *n){
 	if (abs(height(n->right)-height(n->left))<2){
 		return n;
	 }
	 else if (height(n->right)>height(n->left)){
	 	if (height(n->right->right)>height(n->right->left)){
	 		return rotateleft(n);
		 }
		 else{
		 	return rotaterl(n);
		 }
	 }
	 else{
	 	if (height(n->left->left)>height(n->left->right)){
	 		return rotateright(n);
		 }
		 else{
		 	return rotatelr(n);
		 }
	 }
 }
 
static TLDNode *insert(TLDNode *n, TLDNode *current){
 	if (current==NULL){
 		current=n;
	 }
	 else if (strcmp(current->tld,n->tld)<0){
	 	current->right=insert(n,current->right);
	 }
	 else if (strcmp(current->tld,n->tld)>0){
	 	current->left=insert(n,current->left);
	 }
	 else{
	 	current->count+=1;
		free(n->tld);
	 	free(n);
	 }
	 return balance(current);
 }
 
int tldlist_add(TLDList *tld, char *hostname, Date *d){
	if (date_compare(tld->begin,d)<=0 && date_compare(tld->end,d)>=0){
		TLDNode *temp;
		temp=malloc(sizeof(TLDNode));
		temp->left=NULL;
		temp->right=NULL;
		temp->count=1;
		temp->tld=malloc(strlen(strrchr(hostname,'.')+1)*sizeof(char));
		strcpy(temp->tld,strrchr(hostname,'.')+1);
		//temp->tld=strrchr(hostname,'.')+1;
		tld->root=insert(temp,tld->root);
		return 1;	
	}
	else{
		return 0; 
	} 
}

/*
 * tldlist_count returns the number of successful tldlist_add() calls since
 * the creation of the TLDList
 */
 
 long treecount(TLDNode *n){
 	if (n==NULL){
 		return 0;
	 }
	 else {
	 	return treecount(n->left)+treecount(n->right)+n->count;
	 }
 }
 
long tldlist_count(TLDList *tld){
	return treecount(tld->root);
}

/*
 * tldlist_iter_create creates an iterator over the TLDList; returns a pointer
 * to the iterator if successful, NULL if not
 */
 
void traverse(TLDNode *current, TLDIterator *iter){
	if (current!=NULL){
		traverse(current->left,iter);
		while (iter->node!=NULL){
			iter=iter->next;
		}
		iter->next=malloc(sizeof(TLDIterator));
		iter->next->node=NULL;
		iter->next->next=NULL;
		iter->next->prev=iter;
		iter->node=current;
		traverse(current->right,iter);
	}
}
 
TLDIterator *tldlist_iter_create(TLDList *tld){
	TLDIterator *rtn;
	rtn=malloc(sizeof(TLDIterator));
	rtn->node=NULL;
	rtn->next=NULL;
	rtn->prev=NULL;
	traverse(tld->root,rtn);
	TLDIterator *t;
	t=rtn;
	while(t->node!=NULL){
		t=t->next;
	}
	free(t);
	return rtn;
}

/*
 * tldlist_iter_next returns the next element in the list; returns a pointer
 * to the TLDNode if successful, NULL if no more elements to return
 */
TLDNode *tldlist_iter_next(TLDIterator *iter){
	if (iter->node==NULL){
		return NULL;
	}
	else {
                TLDNode *rtn=iter->node;
		//puts(rtn->tld);
		memcpy(iter,iter->next,sizeof(TLDIterator));
		return rtn;
	}
}

/*
 * tldlist_iter_destroy destroys the iterator specified by `iter'
 */

static void distroy_iter(TLDIterator *iter){
	//puts(iter->node->tld);
	if (iter->node==NULL){
		//free(iter);
		return;
	}
	//puts("0");
	//puts(iter->prev->node->tld);
	//if (iter->prev->node==NULL){ puts("1");}
	distroy_iter(iter->prev);
	free(iter);
}

void tldlist_iter_destroy(TLDIterator *iter){
	//puts(iter->prev->node->tld);
	distroy_iter(iter->prev);
	free(iter);
}

/*
 * tldnode_tldname returns the tld associated with the TLDNode
 */
char *tldnode_tldname(TLDNode *node){
	return node->tld;
}

/*
 * tldnode_count returns the number of times that a log entry for the
 * corresponding tld was added to the list
 */
long tldnode_count(TLDNode *node){
	return node->count;
}


