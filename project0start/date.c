//CIS415 PROJECT0
//Yueqi Zhu
//DUCKID: yueqiz
//This is my own work

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct date
{
	int day;
	int month;
	int year;
} Date;

/*
 * date_create creates a Date structure from `datestr`
 * `datestr' is expected to be of the form "dd/mm/yyyy"
 * returns pointer to Date structure if successful,
 *         NULL if not (syntax error)
 */
Date *date_create(char *datestr){
	if (!(strlen(datestr)==10 && datestr[2]=='/' && datestr[5]=='/')){
		return NULL;
	}
	int i;
	for (i=0;i<10;i++){
		if (i==2 || i==5){
			continue;
		}
		if (datestr[i]<'0' || datestr[i]>'9'){
			return NULL;
		}
	}
	Date *rtn;
	rtn=malloc(sizeof(Date));
	rtn->day=(datestr[0]-'0')*10+(datestr[1]-'0');
	rtn->month=(datestr[3]-'0')*10+(datestr[4]-'0');
	rtn->year=(datestr[6]-'0')*1000+(datestr[7]-'0')*100+(datestr[8]-'0')*10+(datestr[9]-'0');
	//printf("%d",rtn.year);
	if (rtn->day>31 || rtn->day<1 || rtn->month>12 || rtn->day<1){
		return NULL;
	}
	else{
		return rtn;
	}
	
	
}

/*
 * date_duplicate creates a duplicate of `d'
 * returns pointer to new Date structure if successful,
 *         NULL if not (memory allocation failure)
 */
Date *date_duplicate(Date *d){
	Date *rtn;
	rtn=malloc(sizeof(Date));
	if (rtn==NULL){
		return NULL;
	}
	rtn->day=d->day;
	rtn->month=d->month;
	rtn->year=d->year;
	return rtn;
}

/*
 * date_compare compares two dates, returning <0, 0, >0 if
 * date1<date2, date1==date2, date1>date2, respectively
 */
int date_compare(Date *date1, Date *date2){
	if (date1->year<date2->year){
		return -1;
	}
	else if (date1->year>date2->year){
		return 1;
	}
	else if (date1->month<date2->month){
		return -1;
	}
	else if(date1->month>date2->month){
		return 1;
	}
	else if (date1->day<date2->day){
		return -1;
	}
	else if(date1->day>date2->day){
		return 1;
	}
	else{
		return 0;
	}
}

/*
 * date_destroy returns any storage associated with `d' to the system
 */
void date_destroy(Date *d){
	free(d);
}

