/*
Yueqi Zhu
yueqiz
CIS415 Project2
This is my own work.
*/

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>

#include "networkdriver.h"
#include "BoundedBuffer.h"
#include "freepacketdescriptorstore__full.h"
#include "packetdescriptorcreator.h"

/*constents*/
#define REPEAT_TIME	 5
#define RBUFFER_SIZE 5
#define SBUFFER_SIZE 5

/* any global variables required for use by your threads and your driver routines */
BoundedBuffer* sbuffer; 
BoundedBuffer* rbuffer[MAX_PID+1];
NetworkDevice* ndevice;
FreePacketDescriptorStore* fpdstore;

/* definition[s] of function[s] required for your thread[s] */

void* send_(){
	int count;
	PacketDescriptor* pdescriptor;
	while (1){
		/*keep trying until there is no block*/
		if (nonblockingReadBB(sbuffer,(void**)&pdescriptor)){
			count=0;
			//try certain times
			while (count<REPEAT_TIME){
				if (send_packet(ndevice,pdescriptor)){
					break;
				}
				count++;
			}
			/*reset packet descriptor*/
			init_packet_descriptor(pdescriptor);
			blocking_put_pd(fpdstore,pdescriptor);
		}
	}
	return 0;
}

void* recieve_(){
	PacketDescriptor* pdescriptor;
	int pid;
	while (1){
		/*obtain a packet from store*/
		blocking_get_pd(fpdstore,&pdescriptor);
		init_packet_descriptor(pdescriptor);
		register_receiving_packetdescriptor(ndevice,pdescriptor);
		await_incoming_packet(ndevice);
		pid=packet_descriptor_get_pid(pdescriptor);
		/*try to write into buffer,  then reset packet descriptor if success*/
		if (!nonblockingWriteBB(rbuffer[pid],pdescriptor)){
			init_packet_descriptor(pdescriptor);
			blocking_put_pd(fpdstore,pdescriptor);
		}
	}
	return 0;
	
}



void init_network_driver(NetworkDevice *nd, void *mem_start,
 unsigned long mem_length, FreePacketDescriptorStore **fpds) {
 /* create Free Packet Descriptor Store */
	*fpds=create_fpds();
 /* load FPDS with packet descriptors constructed from mem_start/mem_length */
	create_free_packet_descriptors(*fpds, mem_start,mem_length);
 /* create any buffers required by your thread[s] */
	sbuffer=createBB(SBUFFER_SIZE);
	int i;
	for (i=0;i<=MAX_PID;i++){
		rbuffer[i]=createBB(RBUFFER_SIZE);
	}
 /* create any threads you require for your implementation */
	pthread_t sthread, rthread;
	pthread_create(&sthread,NULL,send_,NULL);
	pthread_create(&rthread,NULL,recieve_,NULL);
 /* return the FPDS to the code that called you */
	ndevice=nd;
	fpdstore=*fpds;
	
}
void blocking_send_packet(PacketDescriptor *pd) {
 /* queue up packet descriptor for sending */
 /* do not return until it has been successfully queued */
	blockingWriteBB(sbuffer,pd);
}
int nonblocking_send_packet(PacketDescriptor *pd) {
 /* if you are able to queue up packet descriptor immediately, do so and return 1 */
 /* otherwise, return 0 */
	return nonblockingWriteBB(sbuffer,pd);
}
void blocking_get_packet(PacketDescriptor **pd, PID pid) {
 /* wait until there is a packet for `pid’ */
 /* return that packet descriptor to the calling application */
	*pd=(PacketDescriptor*)blockingReadBB(rbuffer[pid]);

}
int nonblocking_get_packet(PacketDescriptor **pd, PID pid) {
 /* if there is currently a waiting packet for `pid’, return that packet */
 /* to the calling application and return 1 for the value of the function */
	return nonblockingReadBB(rbuffer[pid],(void**)pd);
}