#include "workqueue.h"
#include <stdlib.h>

typedef struct node{
	char *data;
	struct node *prev;
	struct node *next;
} Node;

typedef struct ll{
	int size;
	Node* head;
	Node* tail;
} LL;

Node *InitNode(char* d){
	Node* rtn=NULL;
	rtn=(Node*)malloc(sizeof(Node));
	rtn->data=d;
	rtn->prev=NULL;
	rtn->next=NULL;
	return rtn;
}

void FreeNode(Node* n){
	free(n);
}

LL *InitLL(){
	LL *rtn=(LL*)malloc(sizeof(LL));
	Node *head=InitNode("0");
	Node *tail=InitNode("1");
	head->next=tail;
	tail->prev=head;
	rtn->size=0;
	rtn->head=head;
	rtn->tail=head;
	return rtn;
}

Node* RmvFirst(LL* l){
	if (l->size>0){
		Node*t=l->head->next;
		l->head->next=t->next;
		l->head->next->prev=l->head;
		l->size--;
		return t;
	}
	else{
		return NULL;
	}
}

void AddLast(LL* l, char* d){
	Node *n=InitNode(d);
	n->prev=l->tail->prev;
	n->next=l->tail;
	l->tail->prev->next=n;
	l->tail->prev=n;
	l->size++;
}




